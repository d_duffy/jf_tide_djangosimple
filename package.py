#!python
# -*- coding: utf-8 -*-
name = "jf_tide_djangosimple"

version = "0.1.0"

authors = [
    "Darragh Duffy"
]

description = \
    """
    Simple Django project for testing in Docker
    """

requires = [
    "python-3+",
    # "jf_sgtk_copycomp",
    # "tk_core",
    # "jf_core",
    # "jf_utils",
    "Django",
    "djangorestframework",
    "django_filter", 
    "django_cors_headers", 
    "requests",
    "openpyxl",
]

build_command = "python {root}/rezbuild.py {install}"


def commands():
    env.PYTHONPATH.append("{root}/python")


tests = {
    "unit": {
        "command": "python setup.py test",
        "requires": ["python"],
    },
    "lint": {
        "command": "pylint jf_tide_djangosimple --output-format=colorized --fail-under=9",
        "requires": ["pylint"]
    },
}