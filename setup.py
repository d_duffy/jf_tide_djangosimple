#!python
# -*- coding: utf-8 -*-
"""The setup script."""

from setuptools import setup, find_packages

with open("README.md") as readme_file:
    readme = readme_file.read()

with open("CHANGELOG.md") as history_file:
    history = history_file.read()

test_requirements = []

setup(
    author="Darragh Duffy",
    author_email="darragh.duffy@jellyfishpictures.co.uk",
    python_requires=">=3.6",
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Intended Audience :: Developers",
        "Natural Language :: English",
        "Programming Language :: Python",
        "Programming Language :: Python :: 2.7",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
    ],
    description="Simple Django project for testing in Docker",
    long_description=readme + "\n\n" + history,
    include_package_data=True,
    keywords="jf_tide_djangosimple",
    name="jf_tide_djangosimple",
    packages=find_packages(include=["jf_tide_djangosimple", "jf_tide_djangosimple.*"]),
    test_suite="tests",
    url="https://bitbucket.org/jf-pictures/jf_tide_djangosimple",
    version="0.1.0",
    zip_safe=False,
)
