from django.urls import path
from . import views

urlpatterns = [
    path('testapp-endpoint', views.TestApp.as_view(template_name="testapp/testapp_endpoint.html"), ),
]