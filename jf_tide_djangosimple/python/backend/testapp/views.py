from django.shortcuts import render
from django.views.generic import TemplateView
from django.http import HttpResponse

# Create your views here.
class TestApp(TemplateView):

    def post(self, request):
        return render(request, "testapp/testapp_endpoint.html", {})