#!python
# -*- coding: utf-8 -*-
"""Top-level package for JF Tide Djangosimple."""

__author__ = """Darragh Duffy"""
__email__ = "darragh.duffy@jellyfishpictures.co.uk"
__version__ = "0.1.0"
