#!python
# -*- coding: utf-8 -*-
import os
import sys
import shutil
import subprocess


def build(source_path, build_path, install_path, targets):
    """
    Generic Rez Build script for python packages that use the standard
    python distribution tools (i.e. setuptools, setup.py).

    Supports local installs, console and gui scripts defined via entry_points,
    and "develop" mode for local installs.

    """

    install_dir_names = ["python", "bin"]
    install_mode = "install" in targets
    develop_mode = "develop" in targets

    def _build():
        # Clear out old python builds
        py_install_root = os.path.join(source_path, "build", "py_install")
        if os.path.exists(py_install_root):
            shutil.rmtree(py_install_root)

        # Run normal python setup build command
        setup_py = os.path.join(source_path, "setup.py")
        cmds = [
            "python",
            setup_py,
            "install",
            "--root",
            py_install_root,
            "--prefix",
            "rez",
            "-f",
        ]
        subprocess.call(cmds, cwd=source_path)

        # Move/Copy build files into rez-build directory
        build_dirs = [
            ("rez/Lib/site-packages", "python"),
            ("rez/Scripts", "bin"),
        ]
        paths = []
        for src_name, dest_name in build_dirs:
            src = os.path.normpath(os.path.join(py_install_root, src_name))
            dest = os.path.join(build_path, dest_name)
            paths.append((src, dest))
        _copy_trees(paths)

    def _install():
        # Copy build to install location
        paths = []
        for name in install_dir_names:
            src = os.path.join(build_path, name)
            dest = os.path.join(install_path, name)
            paths.append((src, dest))
        _copy_trees(paths)

    def _copy_trees(paths):
        # Utility function to copy a list of directories pairs.
        for src, dest in paths:
            if os.path.exists(dest):
                shutil.rmtree(dest)
            if os.path.exists(src):
                shutil.copytree(src, dest)

    def _develop():
        # Clear out old develop builds
        develop_root = os.path.join(source_path, "build", "py_develop")
        if os.path.exists(develop_root):
            shutil.rmtree(develop_root)

        # Create develop locations, required by setuptools
        python_dir = os.path.join(develop_root, "python")
        bin_dir = os.path.join(develop_root, "bin")
        os.makedirs(python_dir)
        os.makedirs(bin_dir)

        # Temporarily add develop install location to PYTHONPATH, otherwise
        # setuptools will refuse to create .pth files for develop mode.
        pythonpath = os.environ.get("PYTHONPATH", "")
        if pythonpath:
            pythonpath += os.pathsep
        pythonpath += python_dir
        env = os.environ.copy()
        env["PYTHONPATH"] = pythonpath

        # Run normal python setup develop command
        setup_py = os.path.join(source_path, "setup.py")
        cmds = ["python", setup_py, "develop", "-d", python_dir, "-s", bin_dir]
        subprocess.call(cmds, cwd=source_path, env=env)

        # Move/Copy develop files into rez-build directory
        build_dirs = [
            ("python", "python"),
            ("bin", "bin"),
        ]
        paths = []
        for src_name, dest_name in build_dirs:
            src = os.path.normpath(os.path.join(develop_root, src_name))
            dest = os.path.join(build_path, dest_name)
            paths.append((src, dest))

        _copy_trees(paths)

    if develop_mode:
        _develop()
    else:
        _build()

    if install_mode:
        _install()


if __name__ == "__main__":
    build(
        source_path=os.environ["REZ_BUILD_SOURCE_PATH"],
        build_path=os.environ["REZ_BUILD_PATH"],
        install_path=os.environ["REZ_BUILD_INSTALL_PATH"],
        targets=sys.argv[1:],
    )
